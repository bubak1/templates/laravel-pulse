<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (User::where('email', 'admin@admin.com')->exists()) {
            return;
        }

        User::create([
            'name' => 'Test User',
            'email' => 'admin@admin.com',
            'password' => bcrypt('seacret123'),
            'is_admin' => true,
        ]);
    }
}
