<div align="center">
    <h1>Laravel Pulse</h1>
</div>

### :notebook_with_decorative_cover: Summary

-   [Description](#point_right-description)
-   [Frameworks](#point_right-frameworks)
-   [Requirements](#point_right-requirements)
-   [Project setup](#point_right-project-setup)
    -   [Clone project](#clone-project)
    -   [Setup ENV file](#setup-env-file)
-   [Supervisor setup](#point_right-supervisor-setup)
    -   [Supervisor config](#point_right-supervisor-config)
    -   [Supervisor start](#point_right-supervisor-start)
-   [Start project](#point_right-start-project)

<br>

## :point_right: Description

The main purpose of this repository is to try Laravel pulse.

<br>

## :point_right: Frameworks

-   Laravel 11.x
-   InertiaJS
-   VueJS
-   MySQL
-   TailwindCSS 3.x

<br>

## :point_right: Requirements

-   PHP 8.3
-   Git
-   Composer
-   MySQL
-   Npm
-   Node
-   Supervisor

<br>

## :point_right: Project setup

### Clone project

```sh
git clone https://gitlab.com/bubak1/templates/laravel-pulse
```

```sh
cd laravel-pulse
```

### Setup ENV file

-   Copy .env.example file and create .env file

```sh
cp .env.example .env
```

-   open .env file

```sh
APP_NAME=Pulse
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=

LOG_CHANNEL=daily
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

#### `APP_NAME` (required - string)

-   name of project

#### `APP_ENV` (required - local, production)

-   local for local development
-   production for production build

#### `APP_DEBUG` (required - true, false)

-   true for local development
-   false for production

#### `APP_URL` (required - localhost, domain url)

-   local development -> localhost / 127.0.0.1
-   production -> domain url (for example: `APP_URL=https://ludwigtomas.cz/`)

#### `DB_CONNECTION` (required - mysql)

#### `DB_HOST` (required)

#### `DB_PORT` (required)

#### `DB_DATABASE` (required)

#### `DB_USERNAME` (required)

#### `DB_PASSWORD` (required)

<br>

## :point_right: Supervisor setup

There are 2 different processes that need to be run by supervisor

-   <b>pulse_check</b> - check VPS status (Ram, CPU, Disk, etc.)
-   <b>pulse_work</b> - for showing visual representation of the data

<br>

-   First you need to install supervisor

```sh
apt install supervisor
```

-   Usual path to supervisor config file is

```sh
cd /etc/supervisor/conf.d/
```

-   or

```sh
cd /etc/supervisor/supervisord.conf
```

-   Then you need to create new file with <b>.conf</b> extension and paste the following code
-   So for example lets create new file called <b>pulse_check.conf</b> (becase of the project name)

```sh
nano pulse_check.conf
```

-   paste the following <b>config</b>

<br>

## :point_right: Supervisor config

-   ⚠️ <code>important - change the path to your project path</code> ⚠️

```sh
[program:pulse_check]
process_name=%(program_name)s
command=php /var/www/html/artisan pulse:check
directory=/var/www/html
autostart=true
autorestart=true
user=www-data
stopwaitsecs=3600
stderr_logfile=/var/www/html/storage/logs/pulse.log
stdout_logfile=/var/www/html/storage/logs/pulse.log
```

-   Save the file and exit <code>( ctrl + x, ctrl + y, enter)</code>

<br>

## :point_right: Supervisor start

-   Then you need to update supervisor and start the notifier group

```sh
supervisorctl reread all
```

```sh
supervisorctl update all
```

```sh
supervisorctl start all
```

<br>

## :point_right: Start project

```sh
composer i
```

```sh
npm i
```

```sh
php artisan key:generate
```

```sh
php artisan storage:link
```

```sh
php artisan migrate --seed
```

```sh
php artisan serve
```

```sh
php run dev
```

<br>
<br>
<br>

<div align="center">
    <h1>vytvořil:</h1>
    <a href="https://ludwigtomas.cz/">
        Ludwig Tomáš
    </a>
</div>
